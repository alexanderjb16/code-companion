//Global variables.
var firepad = null;
var firepadRef = null;
var currentKey = null;
var currentName = 'No file selected';
var canOpenFile = true;
var editorInitiated = false;
var newName = null;
var newDesc = null;

//Update project status.
function updateProjectStatus(id, path)
{
    var key = currentKey;
    if (!canOpenFile)
    {
        key = null;
    }
    if (key == null)
    {
        key = "null";
    }
    var data = { "file_name": currentName, "key": key, "project_id": id };
    $.ajax(
    {
        type: 'POST',
        url: '/Projects/ProjectStatus',
        data: data,
        success: function (status)
        {
            if (status == 1)
            {
                alertRedirect("The project you are working on has been deleted.", path);
            }
            else if (status == 2)
            {
                alertRedirect("Your access to this project has been removed.", path);
            }
            else if (status == 3)
            {
                refresh(id, true);
                bootbox.alert("The file you are editing has been deleted.");
            }
            else if (status == 4)
            {
                refresh(id, false);
            }
        }
    });
}

//Alert for user if redirected.
function alertRedirect(msg, path)
{
    bootbox.alert(msg, function ()
    {
        window.location.replace(path);
    });
}

//Initialize firebase.
function init(val)
{
    var config =
    {
        apiKey: "AIzaSyCX-x_5LYUSV_1SUlQg0jCB76Z2JGa_-7Y",
        authDomain: "code-companion.firebaseapp.com",
        databaseURL: "https://code-companion.firebaseio.com",
        projectId: "code-companion",
        storageBucket: "code-companion.appspot.com",
        messagingSenderId: "364028153630"
    };
    firebase.initializeApp(config);
}

//Initialize editor.
function initEditor(key, name, text)
{
    //// Get Firebase Database reference.
    firepadRef = firebase.database().ref().child(key);
    //// Create ACE
    var editor = ace.edit("editor");
    editorInitiated = true;
    editor.$blockScrolling = Infinity;
    var theme = localStorage['code_companion_theme'] || 'ace/theme/monokai';
    editor.setTheme(theme);
    editor.setShowPrintMargin(false);
    var session = editor.getSession();
    session.setUseWrapMode(true);
    session.setUseWorker(false);
    var modelist = ace.require("ace/ext/modelist");
    editor.session.setMode(modelist.getModeForPath(name).mode);
    //// Create Firepad.
    if (text != null)
    {
        firepad = Firepad.fromACE(firepadRef, editor,
        {
            defaultText: text
        });
    }
    else
    {
        firepad = Firepad.fromACE(firepadRef, editor,
        {
            defaultText: "Default text"
        });
    }
    //When firepad is ready settings.
    firepad.on('ready', function ()
    {
        canOpenFile = true;
        editor.setReadOnly(false);
        $("#file_buttons").css("display", "block");
    });
}

//
function getNewRef()
{
    return firebase.database().ref().push();
}

//Add new file.

function addFile(id)
{
    bootbox.confirm(
    {
        title: "Add a new file",
        message: "<form id='new_file'>\
                              File name: <input type='text' id='file_name' /><div id='error_msg' class='error'/><br/>\
                          </form><br/>\
                          For example <i>HelloWorld.java</i>",
        buttons:
        {
            cancel:
            {
                label: '<i class="fa fa-times"></i> Cancel'
            },
            confirm:
            {
                className: "btn-primary",
                label: '<i class="fa fa-check"></i> Add'
            }
        },
        callback: function (result)
        {
            if (result)
            {
                var value = $('#file_name').val().trim();
                newFile(value, null, id);
                return false;
            }
        }

    });
}

//Set focus on inputbox when bootbox is shown.
$(document).on("shown.bs.modal", function (event)
{
    $('#file_name').focus();
    $('#user_name').focus();
    $('#Project_Name').focus();
    $('#new_name').focus();
    if (newName != null && newDesc != null)
    {
        $("#Project_Name").val(newName);
        $("#Project_Description").val(newDesc);
    }
});

//Generates the file key, loads the file skeleton then tells the server to create the file
function newFile(name, defaultText, id)
{
    refresh(id, false);
    if (document.getElementById("file_" + name.toLowerCase()))
    {
        $("#error_msg").html("<br/>A file with that name already exists.");
        return;
    }
    var key = getNewRef().key;
    if (defaultText == null)
    {
        $.ajax(
        {
            type: 'GET',
            url: '/Projects/FileSkeleton',
            contentType: "text/plain; charset=utf-8",
            data:
            {
                Name: name
            },
            success: function (result)
            {
                sendFileData(key, name, result, id);
            }
        });
    }
    else
    {
        sendFileData(key, name, defaultText, id);
    }
}

//Send file data to be created on the server
function sendFileData(key, name, text, id)
{
    var data = { "name": name, "key": key, "project_id": id };
    $.ajax(
    {
        type: 'POST',
        url: '/Projects/Edit',
        data: data,
        success: function (result)
        {
            if (result == 1) {
                $("#error_msg").html("<br/>File name can not be longer than 30 characters and must be more than 1 letter!");
                return false;
            }
            else if (result == 2)
            {
                $("#error_msg").html("<br/>Html tags not valid.");
                return false;
            }
            else
            {
                $("#error_msg").html("");
            }
            refresh(id, false);
            openFile(key, name, text);
            bootbox.hideAll();
        }
    });
}

//Open a file in the editor.
function openFile(key, name, defaultText)
{
    if (!canOpenFile)
    {
        $(".file_" + key).css("text-decoration", "none");
        return false;
    }
    if (currentKey != null && currentKey == key)
    {
        return false;
    }
    if (currentKey != null)
    {
        $(".file_" + currentKey).css("text-decoration", "none");
    }
    $("#file_buttons").css("display", "none");
    canOpenFile = false;
    currentKey = key;
    currentName = name;
    if (firepad != null)
    {
        firepad.dispose();
        firepad = null;
    }
    var editor = ace.edit("editor");
    editor.setValue("");
    initEditor(key, name, defaultText);
    $(".current_file").text(projectName + " / " + name);
    $(".data").css("border", "0");
    $(".file_" + key).css("text-decoration", "underline");
    return false;
};

//Refresh file list, if reset is true then reset editor
function refresh(id, reset)
{
    $.ajax(
    {
        type: 'GET',
        url: '/Projects/Edit',
        data:
        {
            Id: id
        },
        success: function (result) {
            $('.files').replaceWith($(result).find('.files'));
            if (!reset)
            {
                if (currentKey != null)
                {
                    $(".file_" + currentKey).css("text-decoration", "underline");
                    currentName = $(".file_" + currentKey).text();
                    $(".current_file").text(projectName + " / " + currentName);
                }
            }
            else
            {
                currentKey = null;
                if (firepad != null)
                {
                    firepadRef.remove();
                    firepad.dispose();
                    firepad = null;
                }
                var editor = ace.edit("editor");
                editor.destroy();
                $('.files').replaceWith($(result).find('.files'));
                $('#editor').replaceWith($(result).find('#editor'));
                $(".data").css("border", "1px solid black");
                $(".current_file").text(projectName + " / No file selected");
                $("#file_buttons").css("display", "none");
            }
        }
    });
}

//Delete project from database
function deleteProject(id, path)
{
    bootbox.confirm(
    {
        message: "Are you sure you want to delete the project? This action cannot be reversed.",
        buttons:
        {
            cancel:
            {
                label: '<i class="fa fa-times"></i> Cancel'
            },
            confirm:
            {
                label: '<i class="fa fa-check"></i> Confirm'
            }
        },
        callback: function (result)
        {
            if (result)
            {
                var data = { "project_id": id };
                $.ajax(
                {
                    type: 'POST',
                    url: '/Projects/GetFiles',
                    data: data,
                    dataType: 'json',
                    success: function (keys)
                    {
                        if (firebase == null)
                        {
                            return;
                        }
                        for (var i = 0; i < keys.length; i++)
                        {
                            firebase.database().ref().child(keys[i]).remove();
                        }
                        var data = { "project_id": id };
                        $.ajax(
                        {
                            type: 'POST',
                            url: '/Projects/DeleteProject',
                            data: data,
                            success: function ()
                            {
                                window.location.replace(path);
                            }
                        });
                    }
                });
            }
        }
    });
}

//Delete file from database.
function deleteFile(id)
{
    if (currentKey == null || !canOpenFile)
    {
        return;
    }
    bootbox.confirm(
        {
        message: "Are you sure you want to delete this file?",
        buttons:
        {
            cancel:
            {
                label: '<i class="fa fa-times"></i> Cancel'
            },
            confirm:
            {
                label: '<i class="fa fa-check"></i> Confirm'
            }
        },
        callback: function (result)
        {
            if (result)
            {
                var data = { "key": currentKey, "project_id": id };
                $.ajax(
                {
                    type: 'POST',
                    url: '/Projects/DeleteFile',
                    data: data,
                    success: function (result)
                    {
                        currentKey = null;
                        if (firepad != null)
                        {
                            firepadRef.remove();
                            firepad.dispose();
                            firepad = null;
                        }
                        var editor = ace.edit("editor");
                        editor.destroy();
                        $('.files').replaceWith($(result).find('.files'));
                        $('#editor').replaceWith($(result).find('#editor'));
                        $(".data").css("border", "1px solid black");
                        $(".current_file").text(projectName + " / No file selected");
                        $("#file_buttons").css("display", "none");
                        bootbox.alert("File deleted.");
                    }
                });
            }
        }
    });
}

//Download current open file to the users computer
//Credits: http://stackoverflow.com/questions/2897619/using-html5-javascript-to-generate-and-save-a-file
function downloadFile()
{
    if (currentKey == null || !canOpenFile)
    {
        return;
    }
    var editor = ace.edit("editor");
    var pom = document.createElement('a');
    pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(editor.getValue()));
    pom.setAttribute('download', currentName);

    if (document.createEvent)
    {
        var event = document.createEvent('MouseEvents');
        event.initEvent('click', true, true);
        pom.dispatchEvent(event);
    }
    else
    {
        pom.click();
    }
}

//Allows the user to leave the project
function leaveProject(id, path)
{
    bootbox.confirm(
        {
        message: "Are you sure you want to leave the project?",
        buttons:
        {
            cancel:
            {
                label: '<i class="fa fa-times"></i> Cancel'
            },
            confirm:
            {
                label: '<i class="fa fa-check"></i> Confirm'
            }
        },
        callback: function (result)
        {
            if (result)
            {
                var data = { "project_id": id };
                $.ajax(
                {
                    type: 'POST',
                    url: '/Projects/LeaveProject',
                    data: data,
                    success: function ()
                    {
                        window.location.replace(path);
                    }
                });
            }
        }
    });
}

//Renames the current open file
function renameFile(id)
{
    if (currentKey == null || !canOpenFile)
    {
        return;
    }
    bootbox.confirm(
    {
        title: "Rename File",
        message: "<form id='new_file_name'> New file name: <input type='text' id='new_name' value='" + currentName + "' /><div id='error_msg' class='error'/><br/><br/>\
						  </form>",
        buttons:
        {
            cancel:
            {
                label: '<i class="fa fa-times"></i> Cancel'
            },
            confirm:
            {
                label: '<i class="fa fa-check"></i> Confirm'
            }
        },
        callback: function (result)
        {
            if (result)
            {
                var name = document.getElementById('new_name').value.trim();
                var data = { "name": name, "key": currentKey };
                $.ajax(
                {
                    type: 'POST',
                    url: '/Projects/RenameFile',
                    data: data,
                    success: function (result)
                    {
                        if (currentName != name && document.getElementById("file_" + name.toLowerCase()))
                        {
                            $("#error_msg").html("<br/>A file with that name already exists.");
                            return;
                        }
                        if (result == 1)
                        {
                            $("#error_msg").html("<br/>File name can not be longer than 30 characters and must be more than 1 letter!");
                            return false;
                        }
                        else if (result == 2)
                        {
                            $("#error_msg").html("<br/>Html tags not valid.");
                            return false;
                        }
                        else
                        {
                            $("#error_msg").html("");
                        }
                        refresh(id, false);
                        $(".current_file").text(projectName + " / " + name);
                        bootbox.hideAll();
                        bootbox.alert("File renamed.");
                    }
                });
                return false;
            }
        }
    });
}

//Prevent default behavior of bootbox button.
$(document).on("submit", ".bootbox form", function (e)
{
    e.preventDefault();
    $(".bootbox .btn-primary").click();
});

//Show bootbox for file sharing. Add and remove users from project.
function share(id, projectOwner)
{
    var data = { "project_id": id };
    $.ajax(
    {
        type: 'POST',
        url: '/Projects/GetUsers',
        data: data,
        dataType: 'json',
        success: function (users)
        {
            var str = "<table border=\"1\" class=\"table table-striped\"><tr><th>Role</th><th>Email</th><th>Remove</th></tr>";
            var name;
            for (var i = 0; i < users.length; i++)
            {
                name = users[i];
                if (users[i] == projectOwner)
                {
                    str += "<tr><td>Owner</td><td>";
                }
                else
                {
                    str += "<tr><td>User</td><td>";
                }
                str += users[i];
                str += "</td>";
                if (users[i] == projectOwner)
                {
                    str += "<td> </td>";
                }
                else
                {
                    str += "<td><a href='#' class='btn btn-xs btn-danger' onclick=\"removeUser('" + name + "', '" + id + "', '" + projectOwner + "'); return false;\" id=\"remove_button\" class=\"fa fa-times\" ><span class='glyphicon glyphicon-remove'></span></a></td>";
                }
                str += "</tr>";
            }
            str += "</table>";
            bootbox.alert(
            {
                title: "Share project",
                message: "Project is shared with the following users: <br />" + str + "\
                              <form id='new_user'> Add user by email: <input type='text' id='user_name' /> <input type='button' class='btn-primary' value='Add' id='add-user-btn' onclick=\"sendShare('" + id + "', '" + projectOwner + "'); return false;\" id=\"add_button\" class=\"fa fa-times\" /></td><div id='error_msg' class='error'/><br/>\
                              </form>",
                buttons:
                {
                    ok:
                    {
                        className: 'btn-default',
                        label: 'Close'
                    }
                }

            });
        }
    });

}

//Share project with user.
function sendShare(id, projectOwner)
{
    var data = { "name": $('#user_name').val(), "project_id": id };
    $.ajax(
    {
        type: 'POST',
        url: '/Projects/Share',
        data: data,
        success: function (status)
        {
            if (status == 0)
            {
                bootbox.hideAll();
                share(id, projectOwner);
            }
            else if (status == 1)
            {
                $("#error_msg").html("<br/>User already in project.");
                return false;
            }
            else if (status == 2)
            {
                $("#error_msg").html("<br/>User does not exist.");
                return false;
            }
            else if (status == 3)
            {
                $("#error_msg").html("<br/>Error!");
                return false;
            }
        }
    });
}

//Remove a user from project.
function removeUser(name, id, projectOwner)
{
    var data = { "name": name, "project_id": id };
    $.ajax(
    {
        type: 'POST',
        url: '/Projects/RemoveUser',
        data: data,
        success: function (remove)
        {
            bootbox.hideAll();
            if (remove == 0)
            {
                share(id, projectOwner);
            }
            else
            {
                bootbox.alert("Error!");
            }
        }
    });
}