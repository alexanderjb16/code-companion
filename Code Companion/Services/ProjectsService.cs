﻿using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Code_Companion.Models;

namespace Code_Companion.Services
{
    public class ProjectsService
    {
        /// <summary>
        /// The database instance.
        /// </summary>
        private readonly IAppDataContext _db;

        /// <summary>
        /// Instance of user manager.
        /// </summary>
        private UserManager<ApplicationUser> manager;

        /// <summary>
        /// A constructor for ProjectsService
        /// </summary>
        public ProjectsService()
        {
            _db = new ApplicationDbContext();
            manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
        }

        /// <summary>
        /// A constructor for ProjectsService
        /// </summary>
        /// <param name="context">IAppDataContext context.</param>
        public ProjectsService(IAppDataContext context)
        {
            _db = context;
            manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
        }

        /// <summary>
        /// Add a project to the database.
        /// </summary>
        /// <param name="project">Project to add.</param>
        public void AddProject(Project project, string email)
        {
            project.ProjectOwner = email;
            _db.Projects.Add(project);
            _db.SaveChanges();
            int projectID = project.ID;
            ProjectUser user = new ProjectUser { ProjectID = projectID, Email = email };
            _db.ProjectUsers.Add(user);
            _db.SaveChanges();
        }

        /// <summary>
        /// Remove a project from database.
        /// </summary>
        /// <param name="project">Project to remove from database</param>
        public void RemoveProject(Project project)
        {
            List<ProjectUser> users = GetUsers(project);
            List<ProjectFile> files = GetFiles(project);
            foreach (ProjectFile f in files)
            {
                _db.ProjectFiles.Remove(f);
            }
            foreach (ProjectUser u in users)
            {
                _db.ProjectUsers.Remove(u);
            }
            _db.SaveChanges();
            _db.Projects.Remove(project);
            _db.SaveChanges();
        }

        /// <summary>
        /// Add a ProjectFile to a project
        /// </summary>
        /// <param name="file">File to add to the project.</param>
        /// <param name="project">Project to add a file to.</param>
        public void AddFile(ProjectFile file, Project project)
        {
            foreach (var f in GetFiles(project))
            {
                if (file.Name.ToLower() == f.Name.ToLower())
                {
                    return;
                }
            }
            _db.ProjectFiles.Add(file);
            _db.SaveChanges();
        }

        /// <summary>
        /// Get a list of projects connected to a user.
        /// </summary>
        /// <param name="email">User email.</param>
        /// <returns>A list of projects.</returns>
        public List<Project> GetProjects(string email)
        {
            return (from p in _db.Projects.ToList()
                    join u in _db.ProjectUsers.ToList()
                    on p.ID equals u.ProjectID
                    where email == u.Email
                    orderby p.Name
                    select p).ToList();
        }

        /// <summary>
        /// Delete a file from database.
        /// </summary>
        /// <param name="key">Key of the file to delete.</param>
        public void DeleteFile(string key)
        {
            ProjectFile file = GetFile(key);
            _db.ProjectFiles.Remove(file);
            _db.SaveChanges();
        }

        /// <summary>
        /// Rename a file.
        /// </summary>
        /// <param name="file">File to rename.</param>
        /// <param name="name">New file name.</param>
        public void RenameFile(ProjectFile file, string name)
        {
            file.Name = name;
            _db.SetModified(file);
            _db.SaveChanges();
        }

        /// <summary>
        /// Get a file by key.
        /// </summary>
        /// <param name="key">File key.</param>
        /// <returns>A file found by key.</returns>
        public ProjectFile GetFile(string key)
        {
            return _db.ProjectFiles.SingleOrDefault(f => f.Key == key);
        }

        /// <summary>
        /// Get a list of users connected to a project.
        /// </summary>
        /// <param name="p">Project.</param>
        /// <returns>Return a list of users connected to a project.</returns>
        public List<ProjectUser> GetUsers(Project p)
        {
            return (from u in _db.ProjectUsers.ToList() where u.ProjectID == p.ID select u).ToList();
        }

        /// <summary>
        /// Get files connected to a project.
        /// </summary>
        /// <param name="p">Project.</param>
        /// <returns>A list of files connected to a project.</returns>
        public List<ProjectFile> GetFiles(Project p)
        {
            return (from f in _db.ProjectFiles.ToList() where f.ProjectID == p.ID select f).ToList();
        }

        /// <summary>
        /// Gets ProjectViewModel for a project connected to user email.
        /// </summary>
        /// <param name="p">Project.</param>
        /// <param name="email">User email.</param>
        /// <returns>ProjectViewModel with all files connected to a project.</returns>
        public ProjectViewModel GetProjectViewModel(Project p, string email)
        {
            return new ProjectViewModel { Project = p, Users = GetUsers(p), Files = GetFiles(p), CurrentUser = email };
        }

        /// <summary>
        /// Gets a list of all ProjectViewModels connected to a user.
        /// </summary>
        /// <param name="email">User email to search for connections to projects.</param>
        /// <param name="projects">Projects.</param>
        /// <returns>A list of all ProjectViewModels connected to a user.</returns>
        public List<ProjectViewModel> GetProjectViewModels(string email, List<Project> projects)
        {
            List<ProjectViewModel> models = new List<ProjectViewModel>();
            foreach (Project p in projects)
            {
                models.Add(GetProjectViewModel(p, email));
            }
            return models;
        }

        /// <summary>
        /// Gets the project by ID.
        /// </summary>
        /// <param name="id">ID of the project to return.</param>
        /// <returns>Returns the project by ID.</returns>
        public Project GetProject(int id)
        {
            return _db.Projects.SingleOrDefault(p => p.ID == id);
        }

        /// <summary>
        /// Gets a list of ProjectRows.
        /// </summary>
        /// <param name="email">User email to find ProjectViewModels</param>
        /// <param name="projects">Projects to make the rows.</param>
        /// <returns>A list of ProjectRows.</returns>
        public List<ProjectRow> GetProjectRows(string email, List<Project> projects)
        {
            List<ProjectViewModel> models = GetProjectViewModels(email, projects);
            List<ProjectRow> projectsRows = new List<ProjectRow>();
            ProjectRow row = new ProjectRow() { Models = new List<ProjectViewModel>() };
            foreach (ProjectViewModel m in models)
            {
                if (row.Models.Count >= 3)
                {
                    projectsRows.Add(row);
                    row = new ProjectRow() { Models = new List<ProjectViewModel>() };
                }
                row.Models.Add(m);
            }
            if (row.Models.Count > 0)
            {
                projectsRows.Add(row);
            }
            return projectsRows;
        }

        /// <summary>
        /// Checks whether a user is in a project.
        /// </summary>
        /// <param name="users">Users to loop through.</param>
        /// <param name="email">Email of the user.</param>
        /// <returns>true if user is in a project, false if he isn't.</returns>
        public bool IsInProject(List<ProjectUser> users, string email)
        {
            foreach (var u in users)
            {
                if (u.Email == email)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Add a user to a project.
        /// </summary>
        /// <param name="project">Project to add user to.</param>
        /// <param name="email">User email to add to the project.</param>
        /// <returns></returns>
        public int AddUserToProject(Project project, string email)
        {
            if (GetUser(email, project.ID) != null)
            {
                return 1;
            }
            if (GetLoginUser(email) == null)
            {
                return 2;
            }
            ProjectUser user = new ProjectUser { ProjectID = project.ID, Email = email };
            _db.ProjectUsers.Add(user);
            _db.SaveChanges();
            return 0;
        }

        /// <summary>
        /// Removes a user from a project.
        /// </summary>
        /// <param name="project">The project to remove user from.</param>
        /// <param name="email">User email.</param>
        /// <returns>A number(Int) for error checking</returns>
        public int RemoveUserFromProject(Project project, string email)
        {
            ProjectUser user = GetUser(email, project.ID);
            if (user != null)
            {
                _db.ProjectUsers.Remove(user);
                _db.SaveChanges();
                return 0;
            }
            else
            {
                return 1;
            }
        }

        /// <summary>
        /// Gets the currently logged in user.
        /// </summary>
        /// <param name="email">user email.</param>
        /// <returns>Currently logged in user.</returns>
        public ApplicationUser GetLoginUser(string email)
        {
            return _db.Users.SingleOrDefault(u => u.Email == email);
        }

        /// <summary>
        /// Returns a single user that is connected to a project.
        /// </summary>
        /// <param name="email">user email will be compared to User.Email from the database.</param>
        /// <param name="projectID">the ID of the project.</param>
        /// <returns></returns>
        public ProjectUser GetUser(string email, int projectID)
        {
            return _db.ProjectUsers.SingleOrDefault(u => u.ProjectID == projectID && u.Email == email);
        }

        /// <summary>
        /// Function to change the name and/or description of the project.
        /// </summary>
        /// <param name="name">The new name.</param>
        /// <param name="desc">The new description.</param>
        /// <param name="project">The project to update.</param>
        public void UpdateProject(string name, string desc, Project project)
        {
            project.Name = name;
            project.Description = desc;
            _db.SetModified(project); 
            _db.SaveChanges();
        }

        /// <summary>
        /// Search for a project connected to the user email which contains the searchString.
        /// </summary>
        /// <param name="email">Parameter containing the user email wich is used to search user projects.</param>
        /// <param name="searchString">String containing the search string.</param>
        /// <returns>Returns a list of projects found.</returns>
        public List<Project> SearchProjects(string email, string searchString)
        {
            if(searchString.Length <= 0)
            {
                return GetProjects(email);
            }
            string search = searchString.ToLower();
            return (from p in _db.Projects.ToList() join u in _db.ProjectUsers.ToList() on p.ID equals u.ProjectID
                    where email == u.Email && p.Name.ToLower().Contains(search) orderby p.Name select p).ToList();
        }
    }
}

