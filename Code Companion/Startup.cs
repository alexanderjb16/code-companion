﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Code_Companion.Startup))]
namespace Code_Companion
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
