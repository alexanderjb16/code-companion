﻿using System.Web.Mvc;

namespace CodeCompanion.Controllers
{
    /// <summary>
    /// The controller for projects.
    /// </summary>
    public class HomeController : Controller
    {

        /// <summary>
        /// A function to see the instruction page
        /// </summary>
        /// <returns>A view.</returns>
        public ActionResult Instructions()
        {
            return View();
        }

    }
}