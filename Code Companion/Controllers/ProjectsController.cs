﻿using System;
using System.IO;
using System.Text;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Microsoft.AspNet.Identity;
using Code_Companion.Models;
using Code_Companion.Services;

namespace CodeCompanion.Controllers
{
    /// <summary>
    /// The controller for projects.
    /// </summary>
    public class ProjectsController : Controller
    {
        /// <summary>
        /// Instance of ProjectsService
        /// </summary>
        private ProjectsService service = new ProjectsService();

        /// <summary>
        /// A function for Index to show the startpage.
        /// </summary>
        /// <returns>A view.</returns>
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return View(service.GetProjectRows(User.Identity.GetUserName(), service.GetProjects(User.Identity.GetUserName())));
            }
            return View();
        }

        /// <summary>
        /// A function for Index when user searches.
        /// </summary>
        /// <param name="collection"></param>
        /// <returns>A view.</returns>
        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            return View(service.GetProjectRows(User.Identity.GetUserName(), service.SearchProjects(User.Identity.GetUserName(), collection["search"])));
        }

        /// <summary>
        /// A function for New()
        /// </summary>
        /// <returns>A view.</returns>
        public ActionResult New()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("Index");
            }
            return View();
        }

        /// <summary>
        /// A function to see the start page.
        /// </summary>
        /// <returns>A view.</returns>
        public ActionResult Home()
        {
            return View();
        }

        /// <summary>
        /// A function to add a new project.
        /// </summary>
        /// <param name="project">New project.</param>
        /// <returns>RedirectToAction Index, project.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken()]
        [ValidateInput(false)]
        public ActionResult New(Project project)
        {
            if (!ModelState.IsValid)
            {
                return null;
            }
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Projects");
            }
            service.AddProject(project, User.Identity.GetUserName());
            return RedirectToAction("Index", "Projects");
        }

        /// <summary>
        /// A function that gets the project by ID.
        /// </summary>
        /// <param name="id">Project ID</param>
        /// <returns>A view with the project.</returns>
        public ActionResult Edit(int? id)
        {
            if (id.HasValue)
            {
                Project project = service.GetProject(id.Value);
                if (project != null)
                {
                    ProjectViewModel model = service.GetProjectViewModel(project, User.Identity.GetUserName());
                    if(service.IsInProject(model.Users, model.CurrentUser))
                    {
                        return View(model);
                    }
                }
            }
            return View();
        }

        /// <summary>
        /// A function that gets the project selected.
        /// </summary>
        /// <param name="collection">A collection containing a file key, project ID and project name.</param>
        /// <returns>A view with the project.</returns>
        [HttpPost]
        [ValidateInput(false)]
        public int Edit(FormCollection collection)
        {
            string key = collection["key"];
            string name = collection["name"].Trim();
            if (name.Length < 1 || name.Length > 30)
            {
                return 1;
            }
            if (Regex.IsMatch(name, @"<[^>]+>"))
            {
                return 2;
            }
            int id = Convert.ToInt32(collection["project_id"]);
            Project project = service.GetProject(id);
            if (project != null)
            {
                if (service.IsInProject(service.GetUsers(project), User.Identity.GetUserName()))
                {
                    service.AddFile(new ProjectFile { Name = name, ProjectID = id, Key = key }, project);
                    return 0;
                }
            }
            return 3;
        }

        /// <summary>
        /// Share a project with user.
        /// </summary>
        /// <param name="collection">A collection containing project ID and user name.</param>
        /// <returns>A number for error checking.</returns>
        [HttpPost]
        [ValidateInput(false)]
        public int Share(FormCollection collection)
        {
            string name = collection["name"];
            int id = Convert.ToInt32(collection["project_id"]);
            Project project = service.GetProject(id);
            if (project != null)
            {
                if (project.ProjectOwner != User.Identity.GetUserName())
                {
                    return 3;
                }
                if (service.IsInProject(service.GetUsers(project), User.Identity.GetUserName()))
                {
                    return service.AddUserToProject(project, name);
                }
            }
            return 3;
        }

        /// <summary>
        /// A function to rename file in the project.
        /// </summary>
        /// <param name="collection">A collection containing file key and new file name.</param>
        /// <returns>Returns new name of file.</returns>
        [HttpPost]
        [ValidateInput(false)]
        public int RenameFile(FormCollection collection)
        {
            string newName = collection["name"].Trim();
            if (newName.Length < 1 || newName.Length > 30)
            {
                return 1;
            }
            if (Regex.IsMatch(newName, @"<[^>]+>"))
            {
                return 2;
            }
            ProjectFile file = service.GetFile(collection["key"]);
            service.RenameFile(file, newName);
            return 0;
        }

        /// <summary>
        /// A function to delete a file from a project.
        /// </summary>
        /// <param name="collection">A collection containing the project ID and file key.</param>
        /// <returns>A ActionResult View Edit if successful else returns null.</returns>
        [HttpPost]
        public ActionResult DeleteFile(FormCollection collection)
        {
            service.DeleteFile(collection["key"]);
            int id = Convert.ToInt32(collection["project_id"]);
            Project project = service.GetProject(id);
            if (project != null)
            {
                ProjectViewModel model = service.GetProjectViewModel(project, User.Identity.GetUserName());
                if (service.IsInProject(model.Users, model.CurrentUser))
                {
                    return View("Edit", model);
                }
            }
            return null;
        }

        /// <summary>
        /// Remove user from project.
        /// </summary>
        /// <param name="collection">A collection containing project ID and user name.</param>
        /// <returns>A number(int). 1 if user is not project owner, 0 if successful.</returns>
        [HttpPost]
        public int RemoveUser(FormCollection collection)
        {
            string name = collection["name"];
            int id = Convert.ToInt32(collection["project_id"]);
            Project project = service.GetProject(id);
            if (project != null)
            {
                if (project.ProjectOwner != User.Identity.GetUserName())
                {
                    return 1;
                }
                if (service.IsInProject(service.GetUsers(project), User.Identity.GetUserName()))
                {
                    service.RemoveUserFromProject(project, name);
                    return 0;
                }
            }
            return 1;
        }

        /// <summary>
        /// Get users connected to project by project ID.
        /// </summary>
        /// <param name="collection">A collection containing the project ID.</param>
        /// <returns>Json list of users connected to a project.</returns>
        [HttpPost]
        public ActionResult GetUsers(FormCollection collection)
        {
            int id = Convert.ToInt32(collection["project_id"]);
            Project project = service.GetProject(id);
            if (project != null)
            {
                var users = service.GetUsers(project);
                if (service.IsInProject(users, User.Identity.GetUserName()))
                {
                    List<string> strUsers = new List<string>();
                    foreach (var u in users)
                    {
                        strUsers.Add(u.Email);
                    }
                    return Json(strUsers);
                }
            }
            return Json(new { });
        }

        /// <summary>
        /// Get files connected to a project by project ID.
        /// </summary>
        /// <param name="collection">A collection containing the project ID.</param>
        /// <returns>Json list of file keys.</returns>
        [HttpPost]
        public ActionResult GetFiles(FormCollection collection)
        {
            int id = Convert.ToInt32(collection["project_id"]);
            Project project = service.GetProject(id);
            if (project != null)
            {
                var users = service.GetUsers(project);
                if (service.IsInProject(users, User.Identity.GetUserName()))
                {
                    List<string> keys = new List<string>();
                    foreach (var f in service.GetFiles(project))
                    {
                        keys.Add(f.Key);
                    }
                    return Json(keys);
                }
            }
            return Json(new { });
        }

        /// <summary>
        /// A function to delete a project.
        /// </summary>
        /// <param name="collection">A collection containing the project ID</param>
        [HttpPost]
        public void DeleteProject(FormCollection collection)
        {
            int id = Convert.ToInt32(collection["project_id"]);
            Project project = service.GetProject(id);
            if (project != null && project.ProjectOwner == User.Identity.GetUserName())
            {
                service.RemoveProject(project);
            }
        }

        /// <summary>
        /// A function which checks the status of the project.
        /// </summary>
        /// <param name="collection"></param>
        /// <returns>A number. 1 if project has been deleted. 3 if file has been deleted. 4 if file has been renamed. 2 if user not in project. 0 if nothing has changed.</returns>
        [HttpPost]
        public int ProjectStatus(FormCollection collection)
        {
            string fileName = collection["file_name"];
            string key = collection["key"];
            int id = Convert.ToInt32(collection["project_id"]);
            Project project = service.GetProject(id);
            if (project == null)
            {
                return 1; // project deleted
            }
            var users = service.GetUsers(project);
            if (service.IsInProject(users, User.Identity.GetUserName()))
            {
                if (key != "null")
                {
                    ProjectFile file = service.GetFile(key);
                    if (file == null)
                    {
                        return 3; // file deleted
                    }
                    if (file.Name != fileName)
                    {
                        return 4; // file renamed
                    }
                }
                return 0; // success
            }
            return 2; // not in project
        }

        /// <summary>
        /// A function for user to leave project by ID.
        /// </summary>
        /// <param name="collection">A collection containing the ID of the project.</param>
        [HttpPost]
        public void LeaveProject(FormCollection collection)
        {
            int id = Convert.ToInt32(collection["project_id"]);
            Project project = service.GetProject(id);
            if (project != null && project.ProjectOwner != User.Identity.GetUserName())
            {
                service.RemoveUserFromProject(project, User.Identity.GetUserName());
            }
        }

        /// <summary>
        /// A function to inject file skeletons into new files based on ending.
        /// </summary>
        /// <param name="name">File name.</param>
        /// <returns>ActionResult containing the content for the new file.</returns>
        [ValidateInput(false)]
        public ActionResult FileSkeleton(string name)
        {
            try
            {
                string content = "";
                if (Regex.IsMatch(name, @"<[^>]+>"))
                {
                    return Content("You created a new file.");
                }
                string fileExtension = Path.GetExtension(name);
                string path = "~/Content/FileSkeletons/" + fileExtension.Replace(".", "") + ".txt";
                if (!System.IO.File.Exists(Server.MapPath(path)))
                {
                    return Content("You created a new file.");
                }
                using (StreamReader streamReader = new StreamReader(Server.MapPath(path), Encoding.UTF8))
                {
                    content = streamReader.ReadToEnd();
                }
                string fileName = name.Substring(0, name.Length - fileExtension.Length);
                content = content.Replace("$program_name", fileName);
                content = content.Replace("$u_program_name", fileName.ToUpper());
                return Content(content);
            }
            catch (Exception e)
            {
                // weird file name so we return the default text, no need to log it
                return Content("You created a new file.");
            }
       
        }

        /// <summary>
        /// A function to update project name and description of a project.
        /// </summary>
        /// <param name="collection">A collection containing the new name an descrition.</param>
        /// <returns>A number for error handling, 1 if user is not project owner, 0 if success. </returns>
        [HttpPost]
        [ValidateInput(false)]
        public int UpdateSettings(FormCollection collection)
        {
            string name = collection["name"].Trim();
            string desc = collection["desc"].Trim();
            if ((name.Length < 1 || name.Length > 30) && (desc.Length < 1 || desc.Length > 255))
            {
                return 4;
            }
            else if (name.Length < 1 || name.Length > 30)
            {
                return 2;
            }
            else if (desc.Length < 1 || desc.Length > 255)
            {
                return 3;
            }
            int id = Convert.ToInt32(collection["project_id"]);
            Project project = service.GetProject(id);
            if (project != null)
            {
                if (project.ProjectOwner != User.Identity.GetUserName())
                {
                    return 1;
                }
                if (service.IsInProject(service.GetUsers(project), User.Identity.GetUserName()))
                {
                    service.UpdateProject(name, desc, project);
                    return 0;
                }
            }
            return 1;
        }

    }
}