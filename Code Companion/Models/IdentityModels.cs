﻿using System;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Code_Companion.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public interface IAppDataContext
    {
        /// <summary>
        /// Database set of projects.
        /// </summary>
        IDbSet<Project> Projects { get; set; }

        /// <summary>
        /// Database set of project users.
        /// </summary>
        IDbSet<ProjectUser> ProjectUsers { get; set; }

        /// <summary>
        /// Database set of project files.
        /// </summary>
        IDbSet<ProjectFile> ProjectFiles { get; set; }

        /// <summary>
        /// Database set of users.
        /// </summary>
        IDbSet<ApplicationUser> Users { get; set; }

        /// <summary>
        /// A function to save changes to the database.
        /// </summary>
        int SaveChanges();

        /// <summary>
        /// A function to modify database.
        /// </summary>
        void SetModified(object entry);
    }


    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IAppDataContext
    {
        /// <summary>
        /// Database set of projects.
        /// </summary>
        public IDbSet<Project> Projects { get; set; }

        /// <summary>
        /// Database set of project users.
        /// </summary>
        public IDbSet<ProjectUser> ProjectUsers { get; set; }

        /// <summary>
        /// Database set of project files.
        /// </summary>
        public IDbSet<ProjectFile> ProjectFiles { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public void SetModified(object entry)
        {
            Entry(entry).State = EntityState.Modified;
        }
    }
}
