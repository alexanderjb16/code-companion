﻿using System.Collections.Generic;

namespace Code_Companion.Models
{
    /// <summary>
    /// Class ProjectRow
    /// </summary>
    public class ProjectRow
    {
        /// <summary>
        /// List of ProjectViewModels in the project row.
        /// </summary>
        public List<ProjectViewModel> Models { get; set; }
    }
}