﻿using System.Collections.Generic;

namespace Code_Companion.Models
{
    /// <summary>
    /// Class ProjectViewModel
    /// </summary>
	public class ProjectViewModel
	{
        /// <summary>
        /// The project in the project view model.
        /// </summary>
        public Project Project { get; set; }

        /// <summary>
        /// A list of users in the project view model.
        /// </summary>
        public List<ProjectUser> Users { get; set; }

        /// <summary>
        /// A list of files in the project view model.
        /// </summary>
        public List<ProjectFile> Files { get; set; }

        /// <summary>
        /// The email of the logged in user.
        /// </summary>
        public string CurrentUser { get; set; }

    }
}