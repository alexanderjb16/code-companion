﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Code_Companion.Models
{
    /// <summary>
    /// Class ProjectUser.
    /// </summary>
    public class ProjectUser
    {
        /// <summary>
        /// ID of the project user.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// ID of the project the user is in.
        /// </summary>
        public int ProjectID { get; set; }

        /// <summary>
        /// Email of the project user.
        /// </summary>
        public string Email { get; set; }
    }
}