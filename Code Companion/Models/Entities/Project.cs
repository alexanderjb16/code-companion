﻿using System.ComponentModel.DataAnnotations;

namespace Code_Companion.Models
{
    /// <summary>
    /// The class Project.
    /// </summary>
    public class Project
    {
        /// <summary>
        /// Project ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Project name. minimun length 1 and maximum length 30. Name field required.
        /// </summary>
        [StringLength(30, MinimumLength = 1, ErrorMessage = "Name can only be between 1 and 30 characters")]
        [Required(ErrorMessage = "Name field is required")]
        public string Name { get; set; }

        /// <summary>
        /// Project owner.
        /// </summary>
        [Display(Name = "Project Owner")]
        public string ProjectOwner { get; set; }

        /// <summary>
        /// Project description.
        /// </summary>
        [StringLength(255, MinimumLength = 1, ErrorMessage = "Description can only be between 1 and 255 characters")]
        [Required(ErrorMessage = "Description field is required")]
        public string Description { get; set; }
    }
}
