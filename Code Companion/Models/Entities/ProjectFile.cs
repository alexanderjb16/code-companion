﻿using System.ComponentModel.DataAnnotations;

namespace Code_Companion.Models
{
    /// <summary>
    /// Class ProjectFile
    /// </summary>
	public class ProjectFile
    {
        /// <summary>
        /// ID of the project file.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// ID of the project the file is in.
        /// </summary>
        public int ProjectID { get; set; }

        /// <summary>
        /// Name of the project file.
        /// </summary>
        [StringLength(30, MinimumLength = 1, ErrorMessage = "Name can only be between 1 and 30 characters")]
        [Required(ErrorMessage = "Name field is required")]
        public string Name { get; set; }

        /// <summary>
        /// File key of the project file.
        /// </summary>
        public string Key { get; set; }

    }
}