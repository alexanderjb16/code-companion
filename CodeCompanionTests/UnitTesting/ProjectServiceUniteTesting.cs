﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Code_Companion.Models;
using Code_Companion.Services;


namespace CodeCompanionTests.Demo
{
    /// <summary>
    /// Unit testing for ProjectService.cs
    /// </summary>
    [TestClass]
    public class UnitTesting
    {
        private ProjectsService projectService;

        /// <summary>
        /// Initializes the testing database and puts users, projects and files into it
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            var fakeDataBase = new FakeDatabase();

            fakeDataBase.Projects.Add(new Project { ID = 1, Description = "this is project 1", Name = "Project 1", ProjectOwner = "kristjanbk16@ru.is" });
            fakeDataBase.Projects.Add(new Project { ID = 2, Description = "this is project 2", Name = "Project 2", ProjectOwner = "alexanderjb16@ru.is" });
            fakeDataBase.Projects.Add(new Project { ID = 3, Description = "this is project 3", Name = "Project 3", ProjectOwner = "audurr16@ru.is" });
            fakeDataBase.Projects.Add(new Project { ID = 4, Description = "this is project 4", Name = "Project 4", ProjectOwner = "thorlakur16@ru.is" });
            fakeDataBase.Projects.Add(new Project { ID = 5, Description = "this is project 5", Name = "Project 5", ProjectOwner = "darri16@ru.is" });
            fakeDataBase.Projects.Add(new Project { ID = 6, Description = "this is project 6", Name = "Project 6", ProjectOwner = "audurr16@ru.is" });
            fakeDataBase.Projects.Add(new Project { ID = 7, Description = "this is project 6", Name = "Project 7", ProjectOwner = "hordurk16@ru.is" });
            fakeDataBase.Projects.Add(new Project { ID = 8, Description = "this is project 8", Name = "Project 8", ProjectOwner = "audurr16@ru.is" });

            fakeDataBase.ProjectFiles.Add(new ProjectFile { ID = 1, Key = "key1", Name = "file1", ProjectID = 1 });
            fakeDataBase.ProjectFiles.Add(new ProjectFile { ID = 2, Key = "key2", Name = "file2", ProjectID = 2 });
            fakeDataBase.ProjectFiles.Add(new ProjectFile { ID = 3, Key = "key3", Name = "file3", ProjectID = 3 });
            fakeDataBase.ProjectFiles.Add(new ProjectFile { ID = 4, Key = "key4", Name = "file4", ProjectID = 3 });
            fakeDataBase.ProjectFiles.Add(new ProjectFile { ID = 5, Key = "key5", Name = "file5", ProjectID = 2 });
            fakeDataBase.ProjectFiles.Add(new ProjectFile { ID = 6, Key = "key6", Name = "file6", ProjectID = 1 });
            fakeDataBase.ProjectFiles.Add(new ProjectFile { ID = 7, Key = "key7", Name = "file7", ProjectID = 4 });
            fakeDataBase.ProjectFiles.Add(new ProjectFile { ID = 8, Key = "key8", Name = "file8", ProjectID = 5 });
            fakeDataBase.ProjectFiles.Add(new ProjectFile { ID = 9, Key = "key9", Name = "file9", ProjectID = 6 });
            fakeDataBase.ProjectFiles.Add(new ProjectFile { ID = 10, Key = "key10", Name = "file10", ProjectID = 6 });
            fakeDataBase.ProjectFiles.Add(new ProjectFile { ID = 11, Key = "key11", Name = "file11", ProjectID = 5 });
            fakeDataBase.ProjectFiles.Add(new ProjectFile { ID = 12, Key = "key12", Name = "file12", ProjectID = 4 });
            fakeDataBase.ProjectFiles.Add(new ProjectFile { ID = 13, Key = "key13", Name = "file13", ProjectID = 7 });
            fakeDataBase.ProjectFiles.Add(new ProjectFile { ID = 14, Key = "key14", Name = "file14", ProjectID = 7 });
            fakeDataBase.ProjectFiles.Add(new ProjectFile { ID = 15, Key = "key15", Name = "file15", ProjectID = 8 });
            fakeDataBase.ProjectFiles.Add(new ProjectFile { ID = 16, Key = "key16", Name = "file16", ProjectID = 8 });
            fakeDataBase.ProjectFiles.Add(new ProjectFile { ID = 17, Key = "key17", Name = "file17", ProjectID = 1 });
            fakeDataBase.ProjectFiles.Add(new ProjectFile { ID = 18, Key = "key18", Name = "file18", ProjectID = 2 });

            fakeDataBase.ProjectUsers.Add(new ProjectUser { ID = 1, Email = "audurr16@ru.is", ProjectID = 1 });
            fakeDataBase.ProjectUsers.Add(new ProjectUser { ID = 2, Email = "alexanderjb16@ru.is", ProjectID = 1 });
            fakeDataBase.ProjectUsers.Add(new ProjectUser { ID = 3, Email = "kristjank16@ru.is", ProjectID = 1 });
            fakeDataBase.ProjectUsers.Add(new ProjectUser { ID = 4, Email = "audurr16@ru.is", ProjectID = 2 });
            fakeDataBase.ProjectUsers.Add(new ProjectUser { ID = 5, Email = "thorlakur16@ru.is", ProjectID = 2 });
            fakeDataBase.ProjectUsers.Add(new ProjectUser { ID = 6, Email = "alexanderjb16@ru.is", ProjectID = 2 });
            fakeDataBase.ProjectUsers.Add(new ProjectUser { ID = 7, Email = "darri16@ru.is", ProjectID = 2 });
            fakeDataBase.ProjectUsers.Add(new ProjectUser { ID = 8, Email = "audurr16@ru.is", ProjectID = 3 });
            fakeDataBase.ProjectUsers.Add(new ProjectUser { ID = 9, Email = "hordurk16@ru.is", ProjectID = 3 });
            fakeDataBase.ProjectUsers.Add(new ProjectUser { ID = 10, Email = "kristjank16@ru.is", ProjectID = 4 });
            fakeDataBase.ProjectUsers.Add(new ProjectUser { ID = 11, Email = "kristjank16@ru.is", ProjectID = 5 });
            fakeDataBase.ProjectUsers.Add(new ProjectUser { ID = 12, Email = "hordurk16@ru.is", ProjectID = 5 });
            fakeDataBase.ProjectUsers.Add(new ProjectUser { ID = 13, Email = "darri16@ru.is", ProjectID = 5 });
            fakeDataBase.ProjectUsers.Add(new ProjectUser { ID = 3, Email = "audurr16@ru.is", ProjectID = 6 });
            fakeDataBase.ProjectUsers.Add(new ProjectUser { ID = 3, Email = "darri16@ru.is", ProjectID = 7 });
            fakeDataBase.ProjectUsers.Add(new ProjectUser { ID = 3, Email = "kristjank16@ru.is", ProjectID = 7 });
            fakeDataBase.ProjectUsers.Add(new ProjectUser { ID = 3, Email = "alexanderjb@ru.is", ProjectID = 8 });
            fakeDataBase.ProjectUsers.Add(new ProjectUser { ID = 3, Email = "darri16@ru.is", ProjectID = 8 });

            fakeDataBase.Users.Add(new ApplicationUser { Email = "audurr16@ru.is", UserName = "audurr16@ru.is" });
            fakeDataBase.Users.Add(new ApplicationUser { Email = "alexanderjb16@ru.is", UserName = "alexanderjb16@ru.is" });
            fakeDataBase.Users.Add(new ApplicationUser { Email = "thorlakur16@ru.is", UserName = "thorlakur16@ru.is" });
            fakeDataBase.Users.Add(new ApplicationUser { Email = "kristjank16@ru.is", UserName = "kristjanbk16@ru.is" });
            fakeDataBase.Users.Add(new ApplicationUser { Email = "hordurk16@ru.is", UserName = "hordurik16@ru.is" });
            fakeDataBase.Users.Add(new ApplicationUser { Email = "darri16@ru.is", UserName = "darri16@ru.is" });

            projectService = new ProjectsService(fakeDataBase);
        }
        /// <summary>
        /// Tests if we can get a of projects belonging to a specific user
        /// </summary>
        [TestMethod]
        public void GetProjectsTest()
        {
            var ids = new HashSet<int>();
            projectService.GetProjects("audurr16@ru.is").ForEach(x => ids.Add(x.ID));
            Assert.AreEqual(4, ids.Count);
            Assert.IsTrue(ids.Contains(1));
            Assert.IsTrue(ids.Contains(2));
            Assert.IsTrue(ids.Contains(3));
            Assert.IsTrue(ids.Contains(6));
            Assert.IsFalse(ids.Contains(4));
            Assert.IsFalse(ids.Contains(5));
            Assert.IsFalse(ids.Contains(7));
            Assert.IsFalse(ids.Contains(8));
        }

        /// <summary>
        /// Tests whether we can make a new project
        /// </summary>
        [TestMethod]
        public void AddProjectTest()
        {
            var result = projectService.GetProject(9);
            Assert.AreEqual(null, result);
            Assert.AreNotEqual(1, result);

            projectService.AddProject((new Project { ID = 9, Description = "this is project 9", Name = "project 9", ProjectOwner = "thorlakur16@ru.is" }), "thorlakur16@ru.is");
            result = projectService.GetProject(9);
            Assert.AreNotEqual(null, result);
            Assert.AreEqual("this is project 9", result.Description);
            Assert.AreNotEqual("not project 9", result.Description);
            Assert.AreEqual("project 9", result.Name);
            Assert.AreNotEqual("p9", result.Name);
            Assert.AreEqual(9, result.ID);
            Assert.AreNotEqual(8, result.ID);
            Assert.AreEqual("thorlakur16@ru.is", result.ProjectOwner);
            Assert.AreNotEqual("audurr16@ru.is", result.ProjectOwner);
        }

        /// <summary>
        /// Tests if we can get a single project by id
        /// </summary>
        [TestMethod]
        public void GetProjectTest()
        {
            var result = projectService.GetProject(3);
            Assert.AreNotEqual(null, result);
            Assert.AreEqual("this is project 3", result.Description);
            Assert.AreNotEqual("this is p3", result.Description);
            Assert.AreEqual("Project 3", result.Name);
            Assert.AreNotEqual("p3", result.Name);
            Assert.AreEqual(3, result.ID);
            Assert.AreNotEqual(8, result.ID);
            Assert.AreEqual("audurr16@ru.is", result.ProjectOwner);
            Assert.AreNotEqual("thorlakur16@ru.is", result.ProjectOwner);
        }

        /// <summary>
        /// Tests if we can remove a single project 
        /// </summary>
        [TestMethod]
        public void RemoveProjectTest()
        {
            var project = projectService.GetProject(3);
            Assert.AreEqual(3, project.ID);

            projectService.RemoveProject(project);

            project = projectService.GetProject(3);
            Assert.AreEqual(null, project);
        }

        /// <summary>
        /// Test if we can make a new file and add it to a project
        /// </summary>
        [TestMethod]
        public void AddFile()
        {
            var result = projectService.GetFiles(new Project { ID = 2 });
            var project = projectService.GetProject(2);
            Assert.AreEqual(3, result.Count);

            projectService.AddFile(new ProjectFile { ID = 19, Key = "key19", Name = "file19", ProjectID = 2 }, project);

            result = projectService.GetFiles(new Project { ID = 2 });
            Assert.AreNotEqual(3, result.Count);
            Assert.AreEqual(4, result.Count);

            var file = projectService.GetFile("key19");
            Assert.AreEqual("file19", file.Name);

            projectService.AddFile(file, project);
            result = projectService.GetFiles(project);

            Assert.AreEqual(4, result.Count);
        }

        /// <summary>
        /// Test if we can get a single file using the file key
        /// </summary>
        [TestMethod]
        public void GetFileTest()
        {
            var result = projectService.GetFile("key6");
            Assert.AreNotEqual(null, result);
            Assert.AreEqual("file6", result.Name);
            Assert.AreNotEqual("p3", result.Name);
            Assert.AreEqual(6, result.ID);
            Assert.AreNotEqual(8, result.ID);
            Assert.AreEqual("key6", result.Key);
            Assert.AreNotEqual("k6", result.Key);
        }

        /// <summary>
        /// Test if we can delete a single file using the file key
        /// </summary>
        [TestMethod]
        public void DeleteFileTest()
        {
            var result = projectService.GetFiles(new Project { ID = 1 });
            Assert.AreEqual(3, result.Count);

            projectService.DeleteFile("key1");

            result = projectService.GetFiles(new Project { ID = 1 });
            Assert.AreEqual(2, result.Count);
        }

        /// <summary>
        /// Tests if we can rename a file
        /// </summary>
        [TestMethod]
        public void RenameFileTest()
        {
            var result = projectService.GetFile("key3");
            Assert.AreSame("file3", result.Name);

            projectService.RenameFile(result, "filefile3");
            Assert.AreNotSame("f3", result.Name);
            Assert.AreSame("filefile3", result.Name);
        }

        /// <summary>
        /// Test if we can get all the users of a single project
        /// </summary>
        [TestMethod]
        public void GetUsersTest()
        {
            var project = projectService.GetProject(3);
            var result = projectService.GetUsers(project);

            Assert.AreEqual(2, result.Count);
            Assert.AreEqual("audurr16@ru.is", project.ProjectOwner);
        }

        /// <summary>
        /// Tests if we can get a single user, of a certain project, using the user's email and the project's id
        /// </summary>
        [TestMethod]
        public void GetUserTest()
        {
            var user = projectService.GetUser(null, 8);
            Assert.AreEqual(user, null);

            var result = projectService.GetUser("audurr16@ru.is", 1);
            Assert.AreSame("audurr16@ru.is", result.Email);
            Assert.AreEqual(1, result.ID);
            Assert.AreNotSame("kristjank16@ru.is", result.Email);
            Assert.AreNotEqual(5, result.ID);
        }

        /// <summary>
        /// Tests if we can get all the files belonging to a certain project
        /// </summary>
        [TestMethod]
        public void GetFilesTest()
        {
            var project = projectService.GetProject(2);
            var resultFiles = projectService.GetFiles(project);

            Assert.AreEqual(3, resultFiles.Count);
            Assert.AreNotEqual(1, resultFiles.Count);

            var anotherProject = projectService.GetProject(3);
            var resultFilesII = projectService.GetFiles(anotherProject);

            Assert.AreEqual(2, resultFilesII.Count);
            Assert.AreNotEqual(7, resultFilesII.Count);
        }

        /// <summary>
        /// Tests if we can get a single project viewModel 
        /// </summary>
        [TestMethod]
        public void GetProjectViewModelTest()
        {
            var project = projectService.GetProject(3);
            var result = projectService.GetProjectViewModel(project, "audurr16@ru.is");
            var users = projectService.GetUsers(project);
            var files = projectService.GetFiles(project);

            Assert.AreSame(project, result.Project);
            Assert.AreEqual(2, users.Count);
            Assert.AreNotEqual(7, users.Count);
            Assert.AreEqual(2, files.Count);
            Assert.AreNotEqual(3, files.Count);
        }

        /// <summary>
        /// Tests if we can get a project viewModels
        /// </summary>
        [TestMethod]
        public void GetProjectViewModelsTest()
        {

            var projects = projectService.GetProjects("audurr16@ru.is");
            var ids = new HashSet<int>();
            projects.ForEach(x => ids.Add(x.ID));

            Assert.IsTrue(ids.Contains(2));
            Assert.IsTrue(ids.Contains(1));
            Assert.IsTrue(ids.Contains(3));
            Assert.IsTrue(ids.Contains(6));
            Assert.IsFalse(ids.Contains(4));
            Assert.IsFalse(ids.Contains(5));
            Assert.IsFalse(ids.Contains(7));
            Assert.IsFalse(ids.Contains(8));
        }

        /// <summary>
        /// Tests if we can get the rows which the user's projects line up in, using the users email to find the projects that belong to him/her
        /// </summary>
        [TestMethod]
        public void GetProjectRowsTest()
        {
            var projects = projectService.GetProjects("audurr16@ru.is");
            var rows = projectService.GetProjectRows("audurr16@ru.is", projects);

            Assert.AreEqual(2, rows.Count);

            projects = projectService.GetProjects("audurr@gmail.com");
            rows = projectService.GetProjectRows("audurr@gmail.com", projects);

            Assert.AreNotEqual(1, rows.Count);
        }

        /// <summary>
        /// Tests if a certain user has access of a certain project
        /// </summary>
        [TestMethod]
        public void isInProjectTest()
        {
            var project = projectService.GetProject(3);
            var users = projectService.GetUsers(project);
            var result = projectService.IsInProject(users, "audurr16@ru.is");
            Assert.IsTrue(result);

            result = projectService.IsInProject(users, "alexanderjb@ru.is");
            Assert.IsFalse(result);
        }

        /// <summary>
        /// Tests if we can share our project by adding another user to it, using the user's email
        /// </summary>
        [TestMethod]
        public void AddUserToProjectTest()
        {
            var project = projectService.GetProject(4);
            var users = projectService.GetUsers(project);
            Assert.AreEqual(1, users.Count);

            projectService.AddUserToProject(project, "thorlakur16@ru.is");
            projectService.AddUserToProject(project, null);
            users = projectService.GetUsers(project);
            Assert.AreEqual(2, users.Count);

            project = projectService.GetProject(4);
            users = projectService.GetUsers(project);
            var user = projectService.GetUser("thorlakur16@ru.is", 4);
            Assert.AreNotEqual("alexanderjb16@ru.is", user.Email);
            Assert.AreEqual(2, users.Count);

            projectService.AddUserToProject(project, "thorlakur16@ru.is");
            projectService.AddUserToProject(project, "audurr15");
            users = projectService.GetUsers(project);
            Assert.AreEqual(2, users.Count);
        }

        /// <summary>
        /// Tests if the owner of a project can remove a user, he/she has invited, from the project using the user´s email
        /// </summary>
        [TestMethod]
        public void RemoveUserFromProjectTest()
        {
            var project = projectService.GetProject(6);
            var users = projectService.GetUsers(project);

            Assert.AreEqual(1, users.Count);

            projectService.RemoveUserFromProject(project, "audurr16@ru.is");

            users = projectService.GetUsers(project);
            Assert.AreNotEqual(1, users.Count);
            Assert.AreEqual(0, users.Count);

            project = projectService.GetProject(7);
            users = projectService.GetUsers(project);

            projectService.RemoveUserFromProject(project, "");
            Assert.AreEqual(2, users.Count);
        }

        /// <summary>
        /// Test if a useraccount exists in our system
        /// </summary>
        [TestMethod]
        public void GetLoginUserTest()
        {
            var user = projectService.GetLoginUser("audurr16@ru.is");
            Assert.AreSame("audurr16@ru.is", user.Email);
        }
        
        /// <summary>
        /// Tests if we can update a single project´s name and/or description
        /// </summary>
        [TestMethod]
        public void UpdateProjectTest()
        {
            var project = projectService.GetProject(2);
            Assert.AreEqual(2, project.ID);
            Assert.AreEqual("this is project 2", project.Description);

            projectService.UpdateProject("project2", "the second project", project);

            project = projectService.GetProject(2);
            Assert.AreEqual("project2", project.Name);
            Assert.AreEqual("the second project", project.Description);
            Assert.AreNotEqual("Project 2", project.Name);
            Assert.AreNotEqual("this is project 2", project.Description);
        }

        /// <summary>
        /// Test if we can search for a project with either part of the name or description
        /// </summary>
        [TestMethod]
        public void SearchProjectsTest()
        {
            var result = projectService.SearchProjects("audurr16@ru.is", "Project 3");

            Assert.AreEqual(1, result.Count);

            result = projectService.SearchProjects("audurr16@ru.is", "6");
            Assert.AreEqual(1, result.Count);

            result = projectService.SearchProjects("audurr16@ru.is", "");
            Assert.AreEqual(4, result.Count);
        }
    }
}