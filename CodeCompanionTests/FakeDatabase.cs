﻿using Code_Companion.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeCompanionTests
{

    /// <summary>
    /// This is an example of how we'd create a fake database by implementing the 
    /// same interface that the BookeStoreEntities class implements.
    /// </summary>
    public class FakeDatabase : IAppDataContext
    {
        /// <summary>
        /// Sets up the fake database.
        /// </summary>
        public FakeDatabase()
        {
            // We're setting our DbSets to be InMemoryDbSets rather than using SQL Server.
            this.Projects = new InMemoryDbSet<Project>();
            this.ProjectFiles = new InMemoryDbSet<ProjectFile>();
            this.ProjectUsers = new InMemoryDbSet<ProjectUser>();
            this.Users = new InMemoryDbSet<ApplicationUser>();
        }

        public IDbSet<Project> Projects { get; set; }
        public IDbSet<ProjectUser> ProjectUsers { get; set; }
        public IDbSet<ProjectFile> ProjectFiles { get; set; }
        public IDbSet<ApplicationUser> Users { get; set; }

        public int SaveChanges()
        {
            // Pretend that each entity gets a database id when we hit save.
            int changes = 0;
            //changes += DbSetHelper.IncrementPrimaryKey<Author>(x => x.AuthorId, this.Authors);
            //changes += DbSetHelper.IncrementPrimaryKey<Book>(x => x.BookId, this.Books);

            return changes;
        }


        public void SetModified(object entry)
        {
        }

        public void Dispose()
        {
            // Do nothing!
        }
    }
}
